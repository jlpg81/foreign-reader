import * as SQLite from 'expo-sqlite';
import { BaseModel, types } from 'expo-sqlite-orm';

export default class Book extends BaseModel {
  constructor(obj) {
    super(obj);
  }

  static get database() {
    return async () => SQLite.openDatabase('database.db');
  }

  static get tableName() {
    return 'books';
  }

  static get columnMapping() {
    return {
      id: { type: types.INTEGER, primary_key: true },
      title: { type: types.TEXT, not_null: true },
      currentPage: { type: types.NUMERIC },
      totalPages: { type: types.NUMERIC },
      bookLanguage: { type: types.TEXT },
      userLanguage: { type: types.TEXT },
      fileLocation: { type: types.TEXT },
      lastAccess: { type: types.INTEGER, default: () => Date.now() },
    };
  }
}
