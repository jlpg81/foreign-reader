import React from 'react';
import { StyleSheet, View } from 'react-native';

import MainMenuNewBook from '../components/MainMenuNewBook';
import MainMenuOldBookList from '../components/MainMenuOldBookList';
import MainMenuTopBar from '../components/MainMenuTopBar';

function MainMenuPage(props) {
  return (
    <View style={styles.mainMenuPageContainer}>
      <MainMenuTopBar />
      <MainMenuNewBook />
      <MainMenuOldBookList />
    </View>
  );
}

const styles = StyleSheet.create({
  mainMenuPageContainer: {
    flex: 1,
    backgroundColor: 'white',
    paddingTop: 10,
  },
});

export default MainMenuPage;
