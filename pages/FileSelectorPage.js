import React from 'react';
import { Text, StyleSheet, View } from 'react-native';
import FileOpenedHistory from '../components/FileOpenedHistory';
import FileSelector from '../components/FileSelector';

function FileSelectorPage() {
  return (
    <View style={styles.fileSelectorPageContainer}>
      <FileSelector />
      <Text>How to add files:</Text>
      <Text> </Text>

      <Text>
        1) This app can only read TXT files. You must previously convert PDF and
        other formats into TXT.
      </Text>
      <Text>
        2) Once converted, the TXT file needs to be cleaned, and pages must be
        separated with the separator (%np by default.)
      </Text>
      <Text>
        3) Once the file has been cleaned, open it with the app. To get a
        translation, click on the word to be translated.
      </Text>
      <Text> </Text>

      <Text>
        If you wish to ask for additional functionalities, you can contact me at
        jlpg81@gmail.com.
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  fileSelectorPageContainer: {
    margin: 10,
  },
});

export default FileSelectorPage;
