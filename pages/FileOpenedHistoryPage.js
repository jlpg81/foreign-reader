import React from 'react';
import { StyleSheet, View } from 'react-native';
import FileOpenedHistory from '../components/FileOpenedHistory';

function FileOpenedHistoryPage() {
  return (
    <View style={styles.fileSelectorPageContainer}>
      <FileOpenedHistory />
    </View>
  );
}

const styles = StyleSheet.create({
  fileSelectorPageContainer: {
    margin: 10,
  },
});

export default FileOpenedHistoryPage;
