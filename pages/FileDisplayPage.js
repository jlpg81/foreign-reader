import React from 'react';
import { View } from 'react-native';

import FileDisplay from '../components/FileDisplay';
import FileTitleDisplay from '../components/FileTitleDisplay';

function FilePage(props) {
  return (
    <View style={{ flex: 1 }}>
      <FileTitleDisplay
        title={props.route.params.title}
        id={props.route.params.id}
        book={props.route.params.book}
      />
      <FileDisplay
        content={props.route.params.content}
        page={props.route.params.page}
        id={props.route.params.id}
        book={props.route.params.book}
      />
    </View>
  );
}

export default FilePage;
