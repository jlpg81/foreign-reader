import React, { useEffect, useState } from 'react';
import {
  Text,
  TextInput,
  TouchableOpacity,
  View,
  StyleSheet,
} from 'react-native';
import Book from '../db';
import { useNavigation } from '@react-navigation/native';

function BookOptions(props) {
  // const [fontSize, setFontSize] = useState(12);
  const [bookLanguage, setBookLanguage] = useState('');
  const [readerLanguage, setReaderLanguage] = useState('');

  const navigation = useNavigation();

  useEffect(() => {
    const currentBook = async () => {
      const result = await Book.find(props.route.params.id);
      return result;
    };
    currentBook().then((res) => {
      setBookLanguage(res.bookLanguage);
      setReaderLanguage(res.userLanguage);
    });
  }, []);

  const saveSettings = async () => {
    const currentBook = await Book.find(props.route.params.id);
    currentBook.bookLanguage = bookLanguage;
    currentBook.userLanguage = readerLanguage;
    currentBook.save();
    navigation.reset({
      index: 0,
      routes: [{ name: 'MainMenuPage' }],
    });
  };

  return (
    <View>
      <Text style={styles.settingsPageTitle}>Book Options</Text>
      <View style={styles.settingsContainer}>
        {/* <Text>Font size</Text> */}
        {/* <TextInput onChangeText={setFontSize} value={fontSize}></TextInput> */}
        <Text style={styles.settingOption}>Book language</Text>
        <TextInput
          onChangeText={setBookLanguage}
          value={bookLanguage}
          style={styles.input}
        ></TextInput>
        <Text style={styles.settingOption}>Reader language</Text>
        <TextInput
          onChangeText={setReaderLanguage}
          value={readerLanguage}
          style={styles.input}
        ></TextInput>
      </View>
      <View style={styles.saveButtonContainer}>
        <TouchableOpacity onPress={saveSettings} style={styles.saveButton}>
          <Text>Save</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  settingsPageTitle: {
    fontSize: 20,
    textAlign: 'center',
    marginTop: 10,
  },
  settingsContainer: {
    margin: 15,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
  },
  settingOption: {
    justifyContent: 'center',
    width: '50%',
    marginTop: 10,
  },
  input: {
    borderColor: 'black',
    borderWidth: 1,
    borderRadius: 5,
    paddingHorizontal: 10,
    marginTop: 10,
  },
  saveButtonContainer: {
    alignItems: 'center',
  },
  saveButton: {
    borderColor: 'black',
    borderWidth: 1,
    borderRadius: 5,
    margin: 5,
    width: 100,
    padding: 5,
    alignItems: 'center',
  },
});

export default BookOptions;
