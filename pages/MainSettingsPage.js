import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { useNavigation } from '@react-navigation/native';

import Book from '../db';

function MainSettingsPage(props) {
  const navigation = useNavigation();

  const cleanDatabase = () => {
    Book.dropTable();
    Book.createTable();
    navigation.reset({
      index: 0,
      routes: [{ name: 'MainMenuPage' }],
    });
  };

  return (
    <View style={styles.mainSettingsPageContainer}>
      <TouchableOpacity onPress={cleanDatabase} style={styles.settingsButton}>
        <Text>Clean Database</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  mainSettingsPageContainer: {
    margin: 10,
    marginTop: 40,
    alignItems: 'center',
  },
  settingsButton: {
    padding: 7,
    borderColor: 'black',
    borderWidth: 1,
    borderRadius: 5,
  },
});

export default MainSettingsPage;
