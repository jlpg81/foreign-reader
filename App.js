// import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';

import StackNavigator from './navigation/StackNavigator';
import Screen from './tools/Screen';

import Book from './db';
Book.createTable();

export default function App() {
  return (
    <Screen>
      <NavigationContainer>
        <StackNavigator />
      </NavigationContainer>
    </Screen>
  );
}
