
Save files previously opened, so they appear in the recent files list.
Open files previously opened by clicking on the list, fetching information from a db or json.
Return the user to the page they left, when they open a book again.
Stop the app from being able to open files other than txt.
Add the option to reset the db/json file, in the global settings menu
Add the option to go to a certain page, in the book menu.
Add the options page for every book, allowing the user to change languages.
When a page changes, we must go to the top of the page.
Show the book title on top, and the options menu.
Add a way to send donations easily.
Add the page to ask for donations.


Devops:
Send the file to google play. It might be better to put my own name, even if SamSam is in the photo.
Make a logo for the app, with Van.
