import React from 'react';
import { StyleSheet, Text } from 'react-native';

function TextUI(props) {
  return <Text style={styles.TextStyle}>{props.children}</Text>;
}

const styles = StyleSheet.create({
  TextStyle: {
    fontSize: 18,
  },
});

export default TextUI;
