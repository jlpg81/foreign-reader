import React from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { useNavigation } from '@react-navigation/native';

import title from '../assets/title.png';
import logo from '../assets/logo.png';

function MainMenuTopBar(props) {
  const navigation = useNavigation();

  const goToSettings = () => {
    navigation.navigate('MainSettingsPage');
  };

  return (
    <View style={styles.topMenuContainer}>
      <Image source={logo} style={styles.logoContainer} />
      <View style={styles.titleLogoContainer}>
        <Image source={title} style={styles.titleContainer} />
        <View style={styles.topRightMenu}>
          <TouchableOpacity
            style={styles.settingsContainer}
            onPress={goToSettings}
          >
            <Text style={styles.topRightMenuItem}>Settings</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  topMenuContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    marginLeft: 5,
  },
  topRightMenu: {
    flexDirection: 'row',
  },
  topRightMenuItem: {
    // fontSize: 10,
    margin: 5,
  },
  titleLogoContainer: {
    flex: 1,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    borderTopColor: 'black',
    borderTopWidth: 1,
    borderBottomColor: 'black',
    borderBottomWidth: 1,
    paddingVertical: 10,
  },
  logoContainer: {
    width: 50,
    height: 50,
  },
  titleContainer: {
    width: 125,
    height: 25,
  },
  settingsContainer: {
    marginRight: 10,
  },
});

export default MainMenuTopBar;
