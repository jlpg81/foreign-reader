import React, { useEffect, useState } from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import Book from '../db';

import MainMenuOldBookItem from './MainMenuOldBookItem';

function MainMenuOldBookList(props) {
  const options = {
    columns:
      'id, title, currentPage, totalPages, bookLanguage,userLanguage, fileLocation, lastAccess',
  };

  const [bookArray, setBookArray] = useState([]);

  useEffect(() => {
    Book.query(options).then((res) => {
      setBookArray(res);
    });
  }, [Book]);

  return (
    <View style={styles.MainMenuOldBookListContainer}>
      <View style={styles.MainMenuOldBookListText}>
        <Text>Last opened:</Text>
      </View>
      {bookArray.length == 0 && (
        <Text style={styles.noFilesOpenedText}>No files opened yet...</Text>
      )}
      {bookArray.slice(0, 4).map((book) => (
        <MainMenuOldBookItem book={book} key={book.id} />
      ))}
      <View style={styles.MainMenuOldBookListText}>
        <TouchableOpacity style={styles.moreBooksButton}>
          <Text>View full library</Text>
        </TouchableOpacity>
      </View>
      <Text style={styles.emailMe}>
        For additional functionalities, email me at: jlpg81@gmail.com
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  MainMenuOldBookListContainer: {
    margin: 5,
    marginTop: 10,
    marginHorizontal: 10,
    borderTopColor: 'black',
    borderTopWidth: 1,
  },
  MainMenuOldBookListText: {
    padding: 5,
    alignItems: 'center',
  },
  noFilesOpenedText: {
    padding: 5,
    textAlign: 'center',
  },
  moreBooksButton: {
    alignItems: 'center',
    padding: 7,
    marginTop: 5,
    borderColor: 'black',
    borderWidth: 1,
    borderRadius: 5,
  },
  emailMe: { marginTop: 5 },
});

export default MainMenuOldBookList;
