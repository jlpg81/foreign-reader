import React from 'react';
import { Text, StyleSheet, View, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { AntDesign } from '@expo/vector-icons';

function MainMenuNewBook(props) {
  const navigation = useNavigation();

  const FileSelectorPageHandler = () => {
    navigation.navigate('FileSelectorPage');
  };

  return (
    <View style={styles.mainMenuListContainer}>
      <Text style={styles.instructions}>
        This app allows readers to translate words or phrases with just a click.
      </Text>

      <View style={styles.menuItemsContainer}>
        <TouchableOpacity
          style={styles.itemContainer}
          onPress={FileSelectorPageHandler}
        >
          <AntDesign name='addfile' size={24} color='black' />
          <Text>New Book</Text>
        </TouchableOpacity>
      </View>

      <Text style={styles.warning}>
        (This app can currently only read TXT files.)
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  mainMenuListContainer: {
    marginTop: 3,
  },
  menuItemsContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 10,
  },
  itemContainer: {
    width: '30%',
    height: 80,
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 6,
    alignItems: 'center',
    marginTop: 10,
  },
  instructions: {
    textAlign: 'justify',
    marginHorizontal: 10,
    marginVertical: 5,
    fontSize: 18,
  },
  warning: {
    textAlign: 'center',
    fontSize: 14,
  },
});

export default MainMenuNewBook;
