import AsyncStorage from '@react-native-async-storage/async-storage';

const generateBookEntry = (res) => {
  const bookId = `${res.name}${res.size}`;
  const bookEntry = { bookId: bookId, title: res.name, page: 0 };
};

const storeData = async (value) => {
  try {
    const localData = JSON.stringify(value);
    await AsyncStorage.setItem('localData', localData);
  } catch (e) {
    // saving error
    console.log('error saving data', e);
  }
};

export default generateBookEntry;
