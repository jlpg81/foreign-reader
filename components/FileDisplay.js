import React from 'react';
import { ScrollView } from 'react-native';

import CurrentPage from './BookContent/CurrentPage';

function FileDisplay(props) {
  const scrollViewRef = React.useRef(null);

  const scroll = () => {
    scrollViewRef.current?.scrollTo({
      x: 0,
      y: 0,
      animated: true,
    });
  };

  return (
    <ScrollView ref={scrollViewRef}>
      <CurrentPage
        content={props.content}
        scroll={scroll}
        pageToOpen={props.page}
        id={props.id}
        book={props.book}
      />
    </ScrollView>
  );
}

export default FileDisplay;
