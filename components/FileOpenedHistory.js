import React, { useEffect, useState } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import Book from '../db';

function FileOpenedHistory(props) {
  const options = {
    columns:
      'id, title, currentPage, fileLocation, lastAccess, userLanguage, bookLanguage',
  };

  const [bookArray, setBookArray] = useState([]);

  // useEffect here allows us to avoid the infinite loop, IMPORTANT
  useEffect(() => {
    Book.query(options).then((res) => {
      setBookArray(res);
    });
  }, []);

  return (
    <View style={styles.halfScreen}>
      <Text>Last Opened files:</Text>
      {bookArray.length == 0 && <Text>No files opened...</Text>}
      {bookArray.map((book) => (
        <TouchableOpacity key={book.title}>
          <Text>{book.title}</Text>
        </TouchableOpacity>
      ))}
    </View>
  );
}

const styles = StyleSheet.create({
  halfScreen: {
    height: Dimensions.get('window').height / 2,
  },
});

export default FileOpenedHistory;
