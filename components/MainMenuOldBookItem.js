import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { AntDesign } from '@expo/vector-icons';
import * as FileSystem from 'expo-file-system';
import { useNavigation } from '@react-navigation/native';

function MainMenuOldBookItem({ book }) {
  const navigation = useNavigation();

  const handleOpenFile = async () => {
    const content = await FileSystem.readAsStringAsync(book.fileLocation);
    navigation.navigate('FileDisplayPage', {
      title: book.title,
      content: content,
      page: book.currentPage,
      id: book.id,
      book: book,
    });
  };

  return (
    <TouchableOpacity key={book.title} onPress={handleOpenFile}>
      <View style={styles.bookItem}>
        <AntDesign name='book' size={24} color='black' />
        <Text>{book.title}</Text>
        <Text>
          {book.currentPage}/{book.totalPages}
        </Text>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  bookItem: {
    margin: 5,
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 7,
    borderColor: 'black',
    borderWidth: 1,
    borderRadius: 5,
  },
});

export default MainMenuOldBookItem;
