import React, { useState } from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import { Icon } from 'react-native-elements';
import * as DocumentPicker from 'expo-document-picker';
import * as FileSystem from 'expo-file-system';

import { useNavigation } from '@react-navigation/native';

// Database
import Book from '../db';

function FileSelector(props) {
  const [fileTitle, setFileTitle] = useState();
  const [file, setFile] = useState();
  const [fileLocation, setFileLocation] = useState();

  const navigation = useNavigation();

  const handleFileSelector = async () => {
    try {
      const res = await DocumentPicker.getDocumentAsync({ type: 'text/plain' });
      setFile(res);
      setFileTitle(res.name);
      setFileLocation('file://' + res.uri);
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw err;
      }
    }
  };

  const handleOpenFile = async () => {
    // check if book is already in database
    const bookFound = await Book.findBy({ title_eq: fileTitle }).then((res) => {
      return res;
    });
    if (bookFound) {
      const content = await FileSystem.readAsStringAsync(fileLocation);

      navigation.navigate('FileDisplayPage', {
        title: fileTitle,
        id: book.id,
        page: book.currentPage,
        book: book,
        content: content,
      });
    } else {
      // if not already in database, construct objet with book data
      const content = await FileSystem.readAsStringAsync(fileLocation);

      const bookToAdd = {
        title: fileTitle,
        currentPage: 1,
        bookLanguage: 'en',
        userLanguage: 'vn',
        fileLocation: fileLocation,
        totalPages: content.split('%np').length,
      };
      // then, save the object to database
      const book = new Book(bookToAdd);
      book.save();
      const newBookId = await Book.findBy({ title_eq: fileTitle }).then(
        (res) => {
          return res.id;
        }
      );

      navigation.navigate('FileDisplayPage', {
        title: fileTitle,
        id: newBookId,
        book: book,
        content: content,
      });
    }
  };

  return (
    <View>
      <Text>Please select the file you wish to open.</Text>
      <View style={styles.grayButton}>
        <TouchableOpacity
          style={styles.folderButton}
          onPress={handleFileSelector}
        >
          <Icon name='folder' type='Entypo' />
        </TouchableOpacity>
      </View>

      {fileTitle && (
        <View>
          <Text>File to Open:</Text>
          <Text>{fileTitle}</Text>
          <View style={styles.grayButton}>
            <TouchableOpacity style={styles.okButton} onPress={handleOpenFile}>
              <Text>Ok</Text>
            </TouchableOpacity>
          </View>
        </View>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  fileSelectorPageContainer: {
    margin: 10,
  },
  folderButton: {
    backgroundColor: 'lightgray',
    height: 50,
    width: 50,
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: '#20232a',
    borderRadius: 6,
    alignItems: 'center',
  },
  grayButton: {
    alignItems: 'center',
    margin: 20,
  },
  okButton: {
    backgroundColor: 'lightgray',
    height: 50,
    width: 50,
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: '#20232a',
    borderRadius: 6,
    alignItems: 'center',
  },
});

export default FileSelector;
