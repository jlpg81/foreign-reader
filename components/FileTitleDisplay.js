import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import { Icon } from 'react-native-elements';
import { useNavigation } from '@react-navigation/native';

function FileTitleDisplay({ title, id }) {
  const navigation = useNavigation();

  const handleOpenBookMenu = async () => {
    navigation.navigate('BookOptions', { id: id });
  };

  return (
    <View style={styles.titleContainer}>
      <Text style={styles.title} numberOfLines={1}>
        {title}
      </Text>
      <TouchableOpacity style={styles.menuContainer}>
        <Icon
          size={40}
          name='menu'
          type='Entypo'
          onPress={handleOpenBookMenu}
        />
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  titleContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomWidth: 2,
    borderBottomColor: 'black',
  },
  title: {
    fontSize: 27,
    flexShrink: 1,
    marginLeft: 5,
  },
  menuContainer: {
    justifyContent: 'center',
  },
});

export default FileTitleDisplay;
