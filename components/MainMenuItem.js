import React from 'react';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native';

function MainMenuItem({ text, component, icon }) {
  const navigation = useNavigation();

  const goToPage = () => {
    navigation.navigate(component);
  };

  return (
    <TouchableOpacity style={styles.itemContainer} onPress={goToPage}>
      <Text>{text}</Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  itemContainer: {
    width: '30%',
    height: 100,
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: 'black',
    borderRadius: 6,
    alignItems: 'center',
    marginTop: 10,
  },
});

export default MainMenuItem;
