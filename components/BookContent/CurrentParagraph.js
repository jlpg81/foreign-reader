import React from 'react';
import { View } from 'react-native';

import CurrentPhrase from './CurrentPhrase';

function CurrentParagraph({ page, book }) {
  const paragraphList = page.split(/\n/);

  return (
    <View>
      {paragraphList.map((paragraph, index) => (
        <CurrentPhrase key={index} paragraph={paragraph} book={book} />
      ))}
    </View>
  );
}

export default CurrentParagraph;
