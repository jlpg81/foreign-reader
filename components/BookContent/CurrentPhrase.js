import React from 'react';
import { StyleSheet, View } from 'react-native';

import CurrentWord from './CurrentWord';

function CurrentPhrase({ paragraph, book }) {
  const replacedPhrases = paragraph
    .replace(/\.\s+/g, '.%nl')
    .replace(/\./g, '.%nl');
  const phrasesList = replacedPhrases.split('%nl');

  return (
    <View style={styles.phraseViewContainer}>
      {phrasesList.map((phrase, index) => (
        <CurrentWord key={index} phrase={phrase} book={book} />
      ))}
    </View>
  );
}

const styles = StyleSheet.create({
  phraseViewContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    // backgroundColor: 'red',
  },
});

export default CurrentPhrase;
