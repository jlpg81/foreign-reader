import React, { useState } from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Modal,
  View,
  Text,
  Pressable,
} from 'react-native';
import TextUI from '../UI/TextUI';
import translate from 'translate-google-api';

function WordItem({ phrase, word, book }) {
  const [modalVisible, setModalVisible] = useState(false);
  const [translatedWord, setTranslatedWord] = useState('');
  const [translatedPhrase, setTranslatedPhrase] = useState('');

  const handleTranslate = async () => {
    const resultPhrase = await translate(phrase, {
      from: book.bookLanguage,
      to: book.userLanguage,
    });
    setTranslatedPhrase(resultPhrase);

    const resultWord = await translate(word, {
      from: book.bookLanguage,
      to: book.userLanguage,
    });
    setTranslatedWord(resultWord);

    setModalVisible(true);
  };

  const B = (props) => (
    <Text style={{ fontWeight: 'bold' }}>{props.children}</Text>
  );

  return (
    <View>
      <Modal
        animationType='slide'
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(!modalVisible);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.modalText}>
              <B>{word}</B> : {translatedWord}
            </Text>
            <Text style={styles.modalText}>
              <B>{phrase}</B>
            </Text>
            <Text style={styles.modalText}>{translatedPhrase}</Text>
            <Pressable
              style={[styles.button, styles.buttonClose]}
              onPress={() => setModalVisible(!modalVisible)}
            >
              <Text style={styles.textStyle}>Close</Text>
            </Pressable>
          </View>
        </View>
      </Modal>

      <TouchableOpacity onPress={handleTranslate}>
        <TextUI>{word} </TextUI>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: '#F194FF',
  },
  buttonClose: {
    backgroundColor: '#2196F3',
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
});

export default WordItem;
