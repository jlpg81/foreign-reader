import React, { useState } from 'react';
import { Text, StyleSheet, View, TouchableOpacity } from 'react-native';
import { Icon } from 'react-native-elements';

import CurrentParagraph from './CurrentParagraph';
import Book from '../../db';

function CurrentPage({ content, scroll, pageToOpen = 1, id, book }) {
  const page = content.split('%np');
  const [currentPage, setCurrentPage] = useState(pageToOpen);

  const nextPageHandler = async () => {
    setCurrentPage(currentPage + 1);
    const currentBook = await Book.find(id);
    currentBook.currentPage = currentPage + 1;
    currentBook.save();
    scroll();
  };

  const previousPageHandler = async () => {
    setCurrentPage(currentPage - 1);
    const currentBook = await Book.find(id);
    currentBook.currentPage = currentPage - 1;
    currentBook.save();
    scroll();
  };

  return (
    <View style={styles.PageView}>
      <CurrentParagraph page={page[currentPage - 1]} book={book} />
      <View style={styles.buttonRow}>
        <View
          style={{
            minWidth: '30%',
          }}
        >
          {currentPage > 1 && (
            <TouchableOpacity
              style={styles.pageChangeButton}
              onPress={previousPageHandler}
            >
              <Icon name='arrow-left' type='FontAwesome' />
              <Text style={{ marginRight: 10 }}>Page {currentPage - 1}</Text>
            </TouchableOpacity>
          )}
        </View>
        <View
          style={{
            justifyContent: 'center',
          }}
        >
          <Text>Page {currentPage}</Text>
        </View>
        <View
          style={{
            minWidth: '30%',
          }}
        >
          {currentPage < page.length && (
            <TouchableOpacity
              style={styles.pageChangeButton}
              onPress={nextPageHandler}
            >
              <Text style={{ marginLeft: 10 }}>Page {currentPage + 1}</Text>
              <Icon name='arrow-right' type='FontAwesome' />
            </TouchableOpacity>
          )}
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  PageView: {
    flex: 1,
    margin: 20,
  },
  pageChangeButton: {
    backgroundColor: 'lightgray',
    height: 30,
    justifyContent: 'center',
    borderWidth: 1,
    borderRadius: 6,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  buttonRow: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: 10,
    justifyContent: 'space-around',
  },
});

export default CurrentPage;
