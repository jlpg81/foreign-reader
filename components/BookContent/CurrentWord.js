import React from 'react';
import { StyleSheet, View } from 'react-native';

import WordItem from './WordItem';

function CurrentWord({ phrase, book }) {
  const words = phrase.split(' ');

  return (
    <>
      <View style={styles.ViewStyle}>
        {words.map((word, index) => (
          <WordItem key={index} phrase={phrase} word={word} book={book} />
        ))}
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  ViewStyle: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    // backgroundColor: 'green',
  },
});

export default CurrentWord;
