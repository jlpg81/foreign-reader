This app was made so that the reader can automatically translate, with just a click, words or phrases into a language he can understand.
It is most useful for language learners who already have a basic grasp of the language, and want to improve their vocabulary and grammar by reading books that interest them.

Instructions:
1) This app can only read TXT files. You must previously convert PDF and other formats into TXT.
2) Once converted, the TXT file needs to be cleaned, and pages must be separated with the separator (%np by default.)
3) Once the file has been cleaned, open it with the app. To get a translation, click on the word to be translated.

If you wish to ask for additional functionalities, you can contact me at jlpg81@gmail.com.