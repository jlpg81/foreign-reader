import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import FileSelectorPage from '../pages/FileSelectorPage';
import FileDisplayPage from '../pages/FileDisplayPage';
import MainMenuPage from '../pages/MainMenuPage';
import BookOptions from '../pages/BookOptions';
import FileOpenedHistoryPage from '../pages/FileOpenedHistoryPage';
import MainSettingsPage from '../pages/MainSettingsPage';

const Stack = createStackNavigator();

function StackNavigator(props) {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name='MainMenuPage'
        component={MainMenuPage}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name='FileSelectorPage'
        component={FileSelectorPage}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name='FileOpenedHistoryPage'
        component={FileOpenedHistoryPage}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name='MainSettingsPage'
        component={MainSettingsPage}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name='FileDisplayPage'
        component={FileDisplayPage}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name='BookOptions'
        component={BookOptions}
        options={{
          headerShown: false,
        }}
      />
    </Stack.Navigator>
  );
}

export default StackNavigator;
